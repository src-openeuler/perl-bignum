Name:      perl-bignum
Version:   0.66
Release:   4
Summary:   transparent big number support
License:   GPL-1.0-or-later or Artistic-1.0-perl
URL:       https://metacpan.org/release/bignum
Source0:   https://cpan.metacpan.org/authors/id/P/PJ/PJACKLAM/bignum-%{version}.tar.gz

BuildArch: noarch
#prereq
BuildRequires: gcc perl(ExtUtils::MakeMaker) perl-interpreter perl-generators
#for test
BuildRequires: perl(Test::More) >= 0.88
Requires:      perl(Carp) >= 1.22
#for test
Requires:      perl(Math::BigInt) >= 2.003002

%description
Three modules provided by this package:
 * bigint - Transparent BigInteger support for Perl
 * bignum - Transparent BigNumber support for Perl
 * bigrat - Transparent BigNumber/BigRational support for Perl
 * bigfloat - Transparent big floating point number support for Perl

%package_help

%prep
%autosetup -n bignum-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot} 
%{_fixperms} %{buildroot}/*

%check
make test TEST_VERBOSE=1

%files
%doc README
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc BUGS CHANGES TODO
%{_mandir}/man3/*

%changelog
* Tue Feb 11 2025 hugel <gengqihu2@h-partners.com> - 0.66-4
- remove requires of the perl-Math-BigRat

* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 0.66-3
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Aug 13 2024 gengqihu <gengqihu2@h-partners.com> - 0.66-2
- License info rectification

* Fri Jul 28 2023 renhongxun <renhongxun@h-partners.com> - 0.66-1
- upgrade version to 0.66

* Tue Oct 25 2022 renhongxun <renhongxun@h-partners.com> - 0.63-2
- Rebuild for next release

* Sun Dec 26 2021 yuanxin <yuanxin24@huawei.com> - 0.63-1
- update version to 0.63

* Sat Jul 25 2020 shixuantong <shixuantong@huawei.com> - 0.51-1
- update to 0.51-1

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.50-4
- Package init
